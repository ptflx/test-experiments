#!/bin/bash

raw_dest="results/is.D"

# Run and save raw results
NPB-CPP/NPB-OMP/bin/is.D > $raw_dest 2>&1
run_status=$?

benchmark_result_dest=results/results.txt 

# Output filtering includes: finding specific line, extracting the value (follows "="), and removing white spaces
cat $raw_dest | grep "Time in seconds" | awk -F "=" '{print $2}' | sed "s/"\ "//g" >> $benchmark_result_dest

exit $run_status
