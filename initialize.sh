#!/bin/bash

# Set up directory for results, delete if already exists
rm -rf results
mkdir results

# Delete old code & build if present
rm -rf NPB-CPP

# Install Packages 
echo -n "Installing Packages "
date
export DEBIAN_FRONTEND=noninteractive
sudo apt-get update -y
# Env Info packages
sudo apt-get install hwinfo numactl -y
# NPB build dependencies
sudo apt-get install gfortran -y
# DVFS dependencies
sudo apt-get install msr-tools cpufrequtils -y
# git
sudo apt-get install git

# Get NPB code
git clone https://github.com/GMAP/NPB-CPP

# Build parallel code
# More on building current NPB: https://github.com/GMAP/NPB-CPP
cd NPB-CPP/NPB-OMP

# Hack for avoiding current build error (TODO: find cleaner solution)
# Error: "g++: error: unrecognized argument in option ‘-mcmodel=medium’"
mv config/make.def config/make-ORIG.def 
cat config/make-ORIG.def  | sed "s/-mcmodel=medium//g" > config/make.def

# make IS CLASS=S
# make IS CLASS=A
make IS CLASS=D
make EP CLASS=A
make LU CLASS=A

printf "NPB build should be complete now. Running one quick test (IS).\n" 
printf "Output below should include build specs & measured numbers.\n"
./bin/is.S

# --------
# Adding setup commands from NPBench (different from NPB)
# More info: https://github.com/spcl/npbench
# --------

# Exit the dir cloned and entered above
cd ..

# Install pip
sudo add-apt-repository universe
sudo apt install python3-pip -y

# NPBench
git clone https://github.com/spcl/npbench.git
cd npbench
python -m pip install -r requirements.txt
python -m pip install .
